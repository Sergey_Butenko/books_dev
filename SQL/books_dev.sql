-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 31 2016 г., 10:18
-- Версия сервера: 5.5.45
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `books_dev`
--

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `book_name`, `book_cover`, `author`, `description`, `publication_year`, `created`, `modified`) VALUES
(1, 'Inferno', '\\upload\\img\\1\\Inferno-dan-brown.jpg', 'Dan Brown', 'Inferno is a 2013 mystery thriller novel by American author Dan Brown and the fourth book in his Robert Langdon series, following Angels & Demons, The Da Vinci Code and The Lost Symbol.[1] The book was released on May 14, 2013 by Doubleday.[2] It was number one on the New York Times Best Seller list for hardcover fiction and Combined Print & E-book fiction for the first eleven weeks of its release, and also remained on the list of E-book fiction for the first seventeen weeks of its release.', 2013, '2016-05-31 07:00:45', '2016-05-31 07:00:45'),
(2, 'PHP. Объекты, шаблоны и методики программирования', '\\upload\\img\\2\\php.jpg', 'Мэт Зандстра', 'Этот материал закладывает основы объектно-ориентированного проектирования и программирования на PHP. Вы изучите также некоторые основополагающие принципы проектирования. В этом издании книги также описаны возможности, появившиеся в PHP версии 5.4, такие как трейты, дополнительные расширения на основе рефлексии, уточнения типов параметров методов, улучшенная обработка исключений и много других мелких расширений языка.', 2015, '2016-05-31 07:06:05', '2016-05-31 07:06:05'),
(3, 'Совершенный код', '\\upload\\img\\3\\1021442775c.gif', 'Стив Макконел', 'Опираясь на академические исследования, с одной стороны, и практический опыт коммерческих разработок ПО - с другой, автор синтезировал из самых эффективных методик и наиболее эффективных принципов ясное прагматичное руководство. Каков бы ни был ваш профессиональный уровень, с какими бы средствами разработками вы ни работали, какова бы ни была сложность вашего проекта, в этой книге вы найдете нужную информацию, она заставит вас размышлять и поможет создать совершенный код. \r\n', 2007, '2016-05-31 07:10:44', '2016-05-31 07:10:44');

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(1, 'test', 'test@mail.ru', '$2y$10$eNZVTJCzQMsxAVg/DfUzTepkyJQJqo5QuReHuWm.ILKviCQ7mzzZa');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
