# My library

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)


The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `git clone https://Sergey_Butenko@bitbucket.org/Sergey_Butenko/books_dev.git`.
3. Run `php composer.phar install`.

If Composer is installed globally, run
```bash
composer install
```
4. Create MySQL database for project.
5. Read and edit `config/app.php` and setup the 'Datasources'
6. Run `bin/cake migrations migrate`.
7. Export MySQL dump, that exists in 'SQL' folder with data to see already existing book from my library or skip this step and create your own.
8. Login using credentials:
username: test
password: test

Or register new user.

You should now be able to visit the path to where you installed the app.

## Troubleshooting

1. Sometimes there is problem with font-awesome. To resolve it, you should download and reload font-awesome.min.css file and font for font-awesome.
