<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$desctiption = 'My library';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $desctiption ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.min') ?>
    <?= $this->Html->css('font-awesome/css/font-awesome.min') ?>
    <?= $this->Html->css('animate') ?>
    <?= $this->Html->css('style') ?>
    <?= $this->Html->css('plugins/footable/footable.core') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<div id="wrapper">
    <?= $this->Flash->render() ?>
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li>
                    <a href="/books/add"><i class="fa fa-book"></i> <span
                            class="nav-label">Create new book</span></a>
                </li>
                <li>
                    <a href="/books/index"><i class="fa fa-list-ol"></i> <span
                            class="nav-label">List of books</span></a>
                </li>
            </ul>
        </div>
    </nav>
    
    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <a href="/users/logout">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>

            </nav>
        </div>

        <?= $this->fetch('content') ?>
    </div>


    <!-- Mainly scripts -->
    <?= $this->Html->script('jquery-2.1.1.js') ?>
    <?= $this->Html->script('bootstrap.min.js') ?>
    <?= $this->Html->script('plugins/metisMenu/jquery.metisMenu.js') ?>
    <?= $this->Html->script('plugins/slimscroll/jquery.slimscroll.min.js') ?>


    <!-- FooTable -->
    <?= $this->Html->script('plugins/footable/footable.all.min.js') ?>

    <!-- Custom and plugin javascript -->
    <?= $this->Html->script('inspinia.js') ?>
    <?= $this->Html->script('plugins/pace/pace.min.js') ?>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function () {

            $('.footable').footable();
            $('.footable2').footable();

            $('#btnCoverPicUpload').click(function () {
                document.getElementById('CoverPic').click();
            });
        });
    </script>
</body>
</html>
