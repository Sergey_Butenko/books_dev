<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Library login</title>

    <?= $this->Html->css('bootstrap.min') ?>
    <?= $this->Html->css('font-awesome/css/font-awesome.min') ?>

    <?= $this->Html->css('animate') ?>
    <?= $this->Html->css('style') ?>

</head>

<body class="gray-bg">
<?= $this->Flash->render() ?>

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">Lib</h1>

        </div>
        <h3>Welcome to my library</h3>
        <p>
            Test work
        </p>
        <p>Login in. To see it in action.</p>
        <form method="post" class="m-t" role="form" action="/users/login">
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Username" required="">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            <p class="text-muted text-center"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="/users/registration">Create an account</a>
        </form>
    </div>
</div>

<!-- Mainly scripts -->
<?= $this->Html->script('jquery-2.1.1.js') ?>
<?= $this->Html->script('bootstrap.min.js') ?>

</body>

</html>
