<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Library registration</title>

    <?= $this->Html->css('bootstrap.min') ?>
    <?= $this->Html->css('font-awesome/css/font-awesome.min') ?>
    <?= $this->Html->css('custom') ?>
    <?= $this->Html->css('animate') ?>
    <?= $this->Html->css('style') ?>

</head>

<body class="gray-bg">
<?= $this->Flash->render() ?>

<div class="middle-box text-center loginscreen   animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">Lib</h1>

        </div>
        <h3>Register to login</h3>
        <p>Create account to see it in action.</p>
        <form method="post" class="m-t" role="form" action="/users/registration">
            <div class="form-group">
                <input type="text" name="username" class="form-control" placeholder="Username" required="">
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" required="">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

            <p class="text-muted text-center"><small>Already have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="/users/login">Login</a>
        </form>
    </div>
</div>

<!-- Mainly scripts -->
<?= $this->Html->script('jquery-2.1.1.js') ?>
<?= $this->Html->script('bootstrap.min.js') ?>

<!-- iCheck -->
<?= $this->Html->script('plugins/iCheck/icheck.min.js') ?>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>
</body>

</html>
