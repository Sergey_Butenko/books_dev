<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Edit book information</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Edit book information</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="close-link">
                        <i class="fa fa-times"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <form method="post" enctype="multipart/form-data" action="/books/edit/<?= $book->id ?>" class="form-horizontal">
                    <div class="form-group"><label class="col-sm-2 control-label">Book cover</label>
                        <div class="col-sm-10">
                            <img src="<?= $book->book_cover ?>" name="tmp_image" id="coverImage" width="200" height="200" >
                            <br>
                            <button type="button" class="btn btn-primary" id="btnCoverPicUpload" style="width: 200px">
                                UPDATE IMAGE
                                <input type="file" name="book_cover" id="CoverPic" style="display:none;"
                                       onchange="uploadTmpImage()"
                                       accept="image/jpeg,image/png,image/jpg">
                            </button>
                        </div>

                    </div>

                    <div class="form-group"><label class="col-sm-2 control-label">Book name</label>
                        <div class="col-sm-10"><input type="text" name="book_name" class="form-control" value="<?= $book->book_name ?>"></div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group"><label class="col-sm-2 control-label">Author</label>
                        <div class="col-sm-10"><input type="text" name="author" class="form-control" value="<?= $book->author ?>"></div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group"><label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10"><textarea name="description" class="form-control"><?= $book->description ?></textarea></div>
                    </div>
                    <div class="hr-line-dashed"></div>

                    <div class="form-group"><label class="col-sm-2 control-label">Publication year</label>
                        <div class="col-sm-10"><input type="text" name="publication_year" class="form-control" value="<?= $book->publication_year ?>"></div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    function uploadTmpImage() {
        var imageData = new FormData($('form')[0]);
        $('img#coverImage').attr('src', 'http://placehold.it/200x200?text=Uploading');

        $.ajax({
            type: "POST",
            url: "<?= $this->Url->build(['controller' => 'Books', 'action' => 'uploadCoverAjax']); ?>",
            data: imageData,
            processData: false,
            contentType: false,
            success: function (resp) {
                $('img#coverImage').attr('src', '\\' + resp.src);
                $('img#coverImage').attr('style', 'width : 200px; height : 200px;');
            }
        });
    }
</script>
