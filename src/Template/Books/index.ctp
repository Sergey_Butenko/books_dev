<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>My books</h2>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List of my books</h5>

                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <input type="text" class="form-control input-sm m-b-xs" id="filter"
                           placeholder="Search in table">

                    <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                        <thead>
                        <tr>
                            <th>Cover</th>
                            <th>Book name</th>
                            <th data-hide="phone,tablet">Author</th>
                            <th data-hide="phone,tablet">Publication year</th>
                            <th data-hide="phone,tablet">Description</th>
                            <th data-hide="phone,tablet">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($books as $book): ?>
                            <tr class="gradeX">
                                <td><img src="<?= $book->book_cover ?>" width="100" height="100"></td>
                                <td><?= h($book->book_name) ?></td>
                                <td><?= h($book->author) ?></td>
                                <td><?= $book->publication_year ?></td>
                                <td><?= $book->description ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $book->id]) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $book->id], ['confirm' => __('Are you sure you want to delete {0}?', $book->book_name)]) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>